

$(function(){ // document ready

$(document).ready(function() {

$('a.mappa').colorbox({iframe:true});

  var paginationBottomOptions = {
    innerWindow: 2,
    outerWindow: 2,
  };

  var options = {
      valueNames: [ 'provincia' ],
      page: 15,
      pagination: true,
    plugins: [
      ListPagination(paginationBottomOptions)
    ]
  };

var infoLista = new List('elenco', options);

infoLista.on('updated', function(elem){
          $('html, body').animate({
               scrollTop: $("#elenco #anchor").offset().top - 100
          }, 1000);  
        if( 0 == elem.matchingItems.length ){
            $('.messages').html("<p class='notFound'>Spiacente, la ricerca non ha prodotto alcun risultato.</p>");
        } else {
            $('.messages').html("");
            };
        })

    $('.ics').click(function() {
      $('.filtro select').prop('selectedIndex',0);
        infoLista.filter(); // Remove all filters
       $(this).css('display','none');
     });


    $('#filter_provincia').change(function() {
      $('.ics').css('display','block');
        var value = this.value;
        console.log(value);
        if(value != 'all') {
        $('.filtro select').not(this).prop('selectedIndex',0);
        //$('.filtro button').removeClass('selected').addClass('all');
        infoLista.filter(function (item) {
            if (item.values().provincia == value) {
                return true;
            } else {
                return false;
            }
        });
        }
        else {
            infoLista.filter(); // Remove all filters
        }
        return false;
     });



});



