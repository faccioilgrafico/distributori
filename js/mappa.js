/*        (function fadeInDiv(){
            var divs = $('.fadeIn');
                var elem = divs.eq(Math.floor(Math.random()*divs.length));
            if (!elem.is(':visible')){
                elem.fadeIn(Math.floor(Math.random()*2000),fadeInDiv); 
            } else {
                elem.fadeOut(Math.floor(Math.random()*2000),fadeInDiv); 
            }
        })();*/

(function fadeInDiv(){
    var divs = $('.fadeIn');
        var elem = divs.eq(Math.floor(Math.random()*divs.length));

    if (!elem.is(':visible')){
        elem.fadeIn(500, fadeInDiv).delay(2000).fadeOut(500);
    } else {
        elem.fadeOut('slow',fadeInDiv); 
    }
})();        